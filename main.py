# Project : Number check is prime
# Professor : vida aghakhanloo
# Student : Seyed Jaffar Esmaili
#Source Link :
from tkinter import *
from tkinter import ttk
from tkinter import messagebox


class App:
    def __init__(self, master):
        self.frame = Frame(master)
        self.frame.config(height=400, width=700, relief='raised', background="#fafafa")
        self.frame.grid()
        self.labelNumber = Label(self.frame, text='لطفا عدد خود را وارد کنید ', background='#fafafa', anchor=E,
                                 font=('Tahoma', 10)).grid(row=1, column=0, columnspan=10, rowspan=3, ipady=10, pady=30)
        self.inputNumber = Entry(self.frame, width=35, justify=RIGHT,borderwidth=1,relief='solid')
        self.inputNumber.grid(row=3, column=0, ipady=2, ipadx=4, pady=0, padx=30, rowspan=1)
        self.button = Button(self.frame, text='چک !', bg='#321a28', fg='#fff', borderwidth=0,
                             command=self.check).grid(row=6, column=0, ipadx=15, ipady=5, pady=5)

    # check numberi s prime
    def check(self):
        if (len(self.inputNumber.get()) == 0):
            messagebox.showerror(title='خطا', message='لطفاعددی را وارد کنید')
        else:
            number= int(self.inputNumber.get())
            x = 0
            for item in range(2, number // 2 + 1):
                if (number % item == 0):
                    x = x + 1
            if (x <= 0):
                messagebox.showinfo(title='موفق', message=" عدد اول است")
            else:
                messagebox.showwarning(title='اطلاعات', message="عدد مورد نظر اول نیست")

def main():
    root = Tk()
    root.geometry("300x200+470+210")
    root.title("Number Is Prime")
    root.resizable(0, 0)
    root.configure(background='#fafafa')
    app = App(root)
    root.mainloop()


if (__name__ == '__main__'): main()
